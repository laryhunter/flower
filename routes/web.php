<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/weather', 'IndexController@weather');

Route::get('/overdue', 'IndexController@overdue');
Route::get('/current', 'IndexController@current');
Route::get('/news', 'IndexController@news');
Route::get('/completed', 'IndexController@completed');

Route::get('/order/{id}', 'OrderController@index');
Route::post('/order/{id}', 'OrderController@store');

Route::get('/products', 'ProductController@index');
Route::post('/products/{id}', 'ProductController@store');