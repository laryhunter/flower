$(document).ready(function () {
    
    $.get('/weather', function(response) {
        $('.weather').html(response.temp);
    }, 'json');
    
    $('.product[contenteditable="true"]').blur(function (event) {
        
        event.stopImmediatePropagation();

        var price = parseInt($(this).html());

        $.post('/products/' + $(this).data('id'), {
            price : price,
            _token: $('#token').val()
        }, function (response) {
            $(this).html(price)
        }, 'json');
    }).keypress(function (event) {
        if(event.which == 13) {
            $(this).blur();
        }
    });
});