@extends('layouts.base') 
@section('content')

<div class="panel-body m-3">
	
	<h1>Order #{{$order->id}}</h1>
	
	<ul class="list-group list-group-horizontal-xl">
		@foreach ($order->products as $item)
			<li class="list-group-item d-flex justify-content-between align-items-center">
				{{ $item->product->name }} 
				<span class="badge badge-primary badge-pill ml-3">{{ $item->quantity }}</span>
			</li>
		@endforeach
	</ul>
	
	<p>Price: {{ $order->products->sum('amount') }}</p>

	{{ Form::open() }} 
    	<div class="form-group">
    		{{ Form::label('email', 'Email') }}
    		{{ Form::text('email', $order->client_email, [
    				'class' => implode(' ', array_merge(['form-control'], $errors->has('email') ? ['is-invalid'] : []))
    			]
    		) }}
    		
    		@if($errors->has('email'))
    			<div class="invalid-feedback">Please choose a username.</div>
    		@endif
    	</div>
    	
    	<div class="form-group">
    		{{ Form::label('partner', 'Partner') }}
    		{{ Form::select('partner', $partners, $order->partner_id, [
    				'class' => implode(' ', array_merge(['form-control'], $errors->has('email') ? ['is-invalid'] : []))
    			]
    		) }}
    	</div>
    	
    	<div class="form-group">
    		{{ Form::label('status', 'Status') }}
    		{{ Form::select('status', App\Order::$statusNames, $order->status, [
    				'class' => implode(' ', array_merge(['form-control'], $errors->has('email') ? ['is-invalid'] : []))
    			]
    		) }}
    	</div>
    	
    	{{ Form::submit('Save', ['class' => 'btn btn-primary']) }}
    	
	{{ Form::close() }}

</div>

@endsection
