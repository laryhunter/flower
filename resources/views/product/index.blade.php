@extends('layouts.base') 

@section('content')

<div class="panel-body m-3">
	<table class="table table-sm">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">Name</th>
				<th scope="col">Vendor</th>
				<th scope="col">Price</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($products as $product)
			<tr>
				<th scope="row">{{ $product->id }}</th>
				<td>{{ $product->name }}</td>
				<td>{{ $product->vendor->name }}</td>
				<td class="product" data-id="{{ $product->id }}" contenteditable="true">{{ $product->price }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	
	<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
	
	{{ $products->links() }}
</div>

@endsection
