<!DOCTYPE html>
<html>
<head>
<title>Laravel Quickstart - Basic</title>

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<div class="collapse navbar-collapse">
			<ul class="navbar-nav">
				<li class="nav-item"><a class="nav-link"
					href='{{ url("/") }}'>Home</a></li>

				<li class="nav-item"><a class="nav-link"
					href='{{ url("/overdue") }}'>Overdue</a></li>
				<li class="nav-item"><a class="nav-link"
					href='{{ url("/current") }}'>Current</a></li>
				<li class="nav-item"><a class="nav-link"
					href='{{ url("/news") }}'>News</a></li>
				<li class="nav-item"><a class="nav-link"
					href='{{ url("/completed") }}'>Completed</a></li>

				<li class="nav-item"><a class="nav-link"
					href='{{ url("/products") }}'>Products</a></li>
					
				<li class="nav-item"><a class="nav-link disabled text-success"
					href='{{ url("/products") }}'><span>Weather: </span><span class="weather">-</span><span>&deg;</span></a></li>


			</ul>
		</div>
	</nav>

	@yield('content')

	<script src="https://code.jquery.com/jquery-3.4.0.min.js"
		integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg="
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"></script>

	<script src="/js/script.js"></script>
</body>
</html>