@extends('layouts.base') 

@section('content')

<div class="panel-body m-3">
	<table class="table table-sm">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">Partner</th>
				<th scope="col">Price</th>
				<th scope="col">Order</th>
				<th scope="col">State</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($orders as $order)
			<tr>
				<th scope="row"><a href='{{ url("/order/{$order->id}") }}'>{{ $order->id }}</a></th>
				<td>{{ $order->partner->name }}</td>
				<td>{{ $order->products->sum('amount') }}</td>
				<td>{{ $order->products->map(function ($item) { return $item->product->name; })->implode(', ')}}</td>
				<td>{{ $order->statusName }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	
	{{ $orders->links() }}
</div>

@endsection
