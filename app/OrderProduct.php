<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{
    
    public function getAmountAttribute()
    {
        return $this->price * $this->quantity;
    }
    
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
