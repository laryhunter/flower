<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{

    /**
     *
     * @param Request $request            
     * @return Response
     */
    public function index(Request $request)
    {
        return view('index', [
            'orders' => Order::paginate(25)
        ]);
    }

    /**
     *
     * @param Request $request            
     * @return Response
     */
    public function overdue(Request $request)
    {
        return view('index', [
            'orders' => Order::where('status', Order::STATUS_CONFIRMED)->where('delivery_dt', '<', DB::raw('NOW()'))
                ->orderBy('delivery_dt', 'DESC')
                ->paginate(25)
        ]);
    }

    /**
     *
     * @param Request $request            
     * @return Response
     */
    public function current(Request $request)
    {
        return view('index', [
            'orders' => Order::where('status', Order::STATUS_CONFIRMED)->where('delivery_dt', '>=', DB::raw('now()'))
                ->where('delivery_dt', '<', DB::raw('DATE_ADD(NOW(), INTERVAL 24 HOUR)'))
                ->orderBy('delivery_dt', 'ASC')
                ->paginate(25)
        ]);
    }

    /**
     *
     * @param Request $request            
     * @return Response
     */
    public function news(Request $request)
    {
        return view('index', [
            'orders' => Order::where('status', Order::STATUS_NEW)->where('delivery_dt', '>=', DB::raw('NOW()'))
                ->orderBy('delivery_dt', 'ASC')
                ->paginate(25)
        ]);
    }

    /**
     *
     * @param Request $request            
     * @return Response
     */
    public function completed(Request $request)
    {
        return view('index', [
            'orders' => Order::where('status', Order::STATUS_COMPLETED)->where(DB::raw('DATE_FORMAT(delivery_dt, "%Y%m%d")'), '=', DB::raw('DATE_FORMAT(NOW(), "%Y%m%d")'))
                ->orderBy('delivery_dt', 'DESC')
                ->paginate(25)
        ]);
    }
    
    /**
     *
     * @param Request $request
     * @return Response
     */
    public function weather(Request $request)
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->get(config('yandex.weather.url'), [
            'query' => [
                'lat' => config('yandex.weather.lat'),
                'lon' => config('yandex.weather.lon')
            ],
            'headers' => [
                'X-Yandex-API-Key' => config('yandex.weather.key'),
            ]
        ]);
        
        $weather = json_decode($response->getBody(), JSON_OBJECT_AS_ARRAY);
        
        return response()->json($weather['fact']);
    }
}