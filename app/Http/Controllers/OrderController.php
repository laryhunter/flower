<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Partner;

class OrderController extends Controller
{

    /**
     *
     * @param Request $request            
     * @param int $id            
     * @return Response
     */
    public function index(Request $request, int $id)
    {
        try {
            $order = Order::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect('/');
        }
        
        return view('order.index', [
            'order' => $order,
            'partners' => Partner::get()->mapWithKeys(function ($item) {
                return [
                    $item->id => $item->name
                ];
            })
        ]);
    }
    
    /**
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function store(Request $request, int $id)
    {
        
        $this->validate($request, [
            'email' => 'required|max:255',
            'partner' => 'required|integer',
            'status' => 'required|integer',
        ]);
        
        try {
            $order = Order::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return redirect('/');
        }
        
        $order->client_email = $request->email;
        $order->partner_id = $request->partner;
        $order->status = $request->status;
        $order->save();
        
        return redirect("/order/{$order->id}");
        
    }
}
