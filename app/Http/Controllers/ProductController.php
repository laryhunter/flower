<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{

    /**
     *
     * @param Request $request            
     * @return Response
     */
    public function index(Request $request)
    {
        return view('product.index', [
            'products' => Product::with('vendor')->orderBy('name', 'ASC')->paginate(25)
        ]);
    }

    /**
     *
     * @param Request $request            
     * @param int $id            
     * @return Response
     */
    public function store(Request $request, int $id)
    {
        $price = filter_input(INPUT_POST, 'price', FILTER_VALIDATE_INT, [
            'options' => [
                'min_range' => 0
            ]
        ]);
        
        if(!$price) {
            return response()->json([
                'success' => false
            ]);
        }
        
        try {
            $product = Product::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'success' => false
            ]);
        }
        
        $product->price = $price;
        $product->save();
        
        return response()->json([
            'success' => true,
            'price' => $price
        ]);
    }
}
