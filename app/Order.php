<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    
    const STATUS_NEW = 0;
    
    const STATUS_CONFIRMED = 10;
    
    const STATUS_COMPLETED = 20;
    
    
    public static $statusNames = [
        self::STATUS_NEW => 'New',
        self::STATUS_CONFIRMED => 'Confirmed',
        self::STATUS_COMPLETED => 'Completed'
    ];
    
    public function getStatusNameAttribute()
    {
        return self::$statusNames[$this->status];
    }
    
    public function partner()
    {
        return $this->belongsTo(Partner::class);
    }
    
    public function products()
    {
        return $this->hasMany(OrderProduct::class);
    }
}
